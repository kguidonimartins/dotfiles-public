;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

;; this mimic the ~/.gitignore file.
;; magit-todos can freeze emacs when handling ~/dotfiles-public repo
((magit-status-mode . ((magit-todos-exclude-globs . (
".android/*" ".authinfo/*" ".cache/*" ".conda/*" ".config/*" ".dir-locals.el/*" ".doom.d/*" ".eclipse/*" ".emacs.d/*" ".emacs.d.centaur/*" ".emacs.d.doom/*" ".emacs.d.vanilla/*" ".emacs.dx/*" ".enrich-classpath-cache/*" ".gitmodules/*" ".gnupg/*" ".java/*" ".local/*" ".m2/*" ".mbsyncrc/*" ".mozilla/*" ".pki/*" ".profile/*" ".R/*" ".screenlayout/*" ".slime/*" ".ssh/*" ".ssr/*" ".stremio-server/*" ".swt/*" ".TinyTeX/*" ".tor-browser/*" ".visidata/*" ".visidatarc/*" ".Xauthority/*" ".xinitrc/*" ".xprofile/*" ".zotero/*" ".zprofile/*" "bin/*" "dotfiles-private/*" "dotfiles-public/*" "dropbox/*" "google-drive/*" "LICENSE/*" "onedrive/*" "README/*" "spark/*" "tmp/*" "todo.txt/*" "Zotero/*"
))))
;; you need to activate this also with:
;; (add-hook 'before-save-hook 'time-stamp)
(ess-mode . ((time-stamp-pattern . "30/^## Última modificação: %%$")
              (time-stamp-format . "%Y-%m-%dT%H%M%S")))
 (markdown-mode . ((time-stamp-pattern . "30/Última modificação: %%$")
                   (time-stamp-format . "%Y-%m-%dT%H%M%S")))
 (text-mode . ((time-stamp-pattern . "30/Última modificação: %%$")
                   (time-stamp-format . "%Y-%m-%dT%H%M%S")))
 )
