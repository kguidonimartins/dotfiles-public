from qgis.core import QgsProject
import os
import shutil
from processing.core.outputs import OutputVector, OutputRaster, OutputFile

project_home_path = QgsProject.instance().homePath()

project_data_temp_path = project_home_path + "/data/temp/qgis-temp/"

# checking if the directory demo_folder 
# exist or not.
if not os.path.exists(project_home_path):
    # if the demo_folder directory is not present 
    # then create it.
    os.makedirs(project_home_path)

for output in alg.outputs:
    if isinstance(output, (OutputVector, OutputRaster, OutputFile)):
        dirname = os.path.split(output.value)[0]
        shutil.copytree(dirname, project_data_temp_path)


