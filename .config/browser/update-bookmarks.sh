#!/usr/bin/env bash

grep -r --include "*.html" "href" ~/google-drive/kguidonimartins/git-repos/startpage | \
    grep "https" | \
    sed -r 's/.*href="([^"]+).*/\1/g' | \
    uniq > ./bookmarks
