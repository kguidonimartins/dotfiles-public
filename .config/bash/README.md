# Moving .bashrc from $HOME to $XDG_CONFIG_HOME/bash

Put this at the bottom of `/etc/bash.bashrc`:

```bash
if [ -s "${XDG_CONFIG_HOME:-$HOME/.config}/bash/bashrc" ]; then
    . "${XDG_CONFIG_HOME:-$HOME/.config}/bash/bashrc"
 fi
```

## Get the default keybindings

```bash
man bash | awk '/^   Commands for Moving$/{print_this=1} /^   Programmable Completion$/{print_this=0} print_this==1{sub(/^   /,""); print}' > bash_commands.txt
```
