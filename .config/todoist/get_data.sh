#!/usr/bin/env bash

todoist --csv --header --indent --namespace --project-namespace list | grep "#DOCTORATE:Chapter 02" | awk -F ',' '{ print $4, $6, $7 }'
