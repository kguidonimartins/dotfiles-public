import subprocess
import pandas as pd

shell_cmd = "todoist --csv --header --indent --namespace --project-namespace list"

with open("todoist_py.csv", "w") as f:
    process = subprocess.Popen(shell_cmd.split(), stdout=f)

fetched_data = pd.read_csv("todoist_py.csv")

fetched_data
