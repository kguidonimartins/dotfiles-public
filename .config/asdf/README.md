# `asdf`

`asdf` é uma excelente ferramenta para manejar diferentes versões de linguagens.
No entanto, eu a descobri depois de instalar todo o meu sistema e adequá-lo às
linguagens *system-wide*. Inclusive esse uso me rendeu dores de cabeça com a
atualização do python para 3.9.
Vou deixar os arquivos de configuração aqui para dar uma chance ao `asdf` em
outra reinstalação do meu sistema.


## Instalação

Veja: https://asdf-vm.com/#/core-manage-asdf?id=install

```bash
git clone https://github.com/asdf-vm/asdf.git ~/.asdf
cd ~/.asdf
git checkout "$(git describe --abbrev=0 --tags)"
```

## Adicionando as linguagens

Tive problemas com as versões 4 do R. Mas foram resolvidas com macetes. Estou
inscrito nessas issues do github para receber as atualizações sobre esse
problema.

### python

```bash
asdf plugin-list-all
asdf plugin-add python
asdf list-all python
asdf install python 3.8.6
asdf global python 3.8.6
```

### R

NOTE: ainda não testei o local de instalação dos pacotes. Eu tenho isso
definido em ~/.config/R/Renviron para a versão 4. Portanto, eu não sei como vai
funcionar para as outras versões. Talvez exista uma solução aqui (ainda não
testei): https://github.com/iroddis/asdf-R/issues/13#issuecomment-744387137

NOTE: não sei dizer se isso é de fato necessário para o R.

```bash
asdf plugin-add R
asdf list-all R # não vai listar as versões 4
asdf install R 4.0.3 # mas a instalação funciona
asdf global R 4.0.3
```

## TODO

Listar as outras linguagens:

- go `go version go1.15.6 linux/amd64`

- rust `cargo 1.48.0`

- node `v15.3.0`

- npm

- yarn

- gem (ruby) `3.1.4`

## Recursos para implementação

https://www.akitaonrails.com/2017/10/24/replacing-rvm-rbenv-nvm-etc-for-asdf

## TODO

Implementar uma rotina parecida com a do plug (vim) e zplug (zsh) para o download
automático em novas instalações.

Não clonar o repositório na $HOME
