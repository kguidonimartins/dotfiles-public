# # always inside tmux
# _not_inside_tmux() { [[ -z "$TMUX" ]] }
# ensure_tmux_is_running() {
#   if _not_inside_tmux; then
#     sh ~/.local/bin/tools/tmux/tat
#   fi
# }
# ensure_tmux_is_running

. /usr/share/LS_COLORS/dircolors.sh

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.config/zsh/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# export TERM="xterm-256color"

# Enable colors and change prompt:
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "


# # History in cache directory:
# HISTFILE=~/.cache/zsh_history
# HISTSIZE=10000
# SAVEHIST=10000

# https://www.reddit.com/r/zsh/comments/kpuggh/slow_zsh_startup_time/
function run_compinit() {
  # references:
  # https://github.com/sorin-ionescu/prezto/blob/master/modules/completion/init.zsh#L31-L44
  # http://zsh.sourceforge.net/Doc/Release/Completion-System.html#Use-of-compinit
  # https://gist.github.com/ctechols/ca1035271ad134841284#gistcomment-2894219
  # https://htr3n.github.io/2018/07/faster-zsh/

  # run compinit in a smarter, faster way
  setopt localoptions extendedglob
  ZSH_COMPDUMP=${ZSH_COMPDUMP:-${XDG_CACHE_HOME:-$HOME/.cache}/zsh/zcompdump}
  autoload -Uz compinit
  # Glob magic explained:
  #   #q expands globs in conditional expressions
  #   N - sets null_glob option (no error on 0 results)
  #   mh-20 - modified less than 20 hours ago
  if [[ $ZSH_COMPDUMP(#qNmh-20) ]]; then
    # -C (skip function check) implies -i (skip security check).
    compinit -C -d "$ZSH_COMPDUMP"
  else
    mkdir -p "$ZSH_COMPDUMP:h"
    compinit -i -d "$ZSH_COMPDUMP"
  fi

  # Compile zcompdump, if modified, in background to increase startup speed.
  {
    if [[ -s "$ZSH_COMPDUMP" && (! -s "${ZSH_COMPDUMP}.zwc" || "$ZSH_COMPDUMP" -nt "${ZSH_COMPDUMP}.zwc") ]]; then
      zcompile "$ZSH_COMPDUMP"
    fi
  } &!
}

run_compinit

autoload -U colors && colors  # Load colors
# autoload -U compinit
# autoload -U compinit && compinit
autoload edit-command-line; zle -N edit-command-line # Edit line in vim with ctrl-e:
zmodload zsh/complist
zmodload -i zsh/complist
# compinit

# following xdg_base
compinit -d "$XDG_CACHE_HOME/zsh/zcompdump-$ZSH_VERSION"
_comp_options+=(globdots)	  # Include hidden files.

zstyle ':completion:*' menu select
zstyle ':completion:*' list-colors 'di=34:ln=35:so=32:pi=33:ex=31:bd=46;34:cd=43;34:su=41;30:sg=46;30:tw=42;30:ow=43;30'
zstyle ':completion:*' list-prompt '%SAt %p: Hit TAB for more, or the character to insert%s'
zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

setopt appendhistory
setopt autocd                     # Automatically cd into typed directory.
setopt append_history         # Allow multiple terminal sessions to all append to one zsh command history
# setopt extended_history       # save timestamp of command and duration
setopt inc_append_history     # Add comamnds as they are typed, don't wait until shell exit
setopt histignorealldups
setopt hist_expire_dups_first # when trimming history, lose oldest duplicates first
setopt hist_ignore_dups       # Do not write events to history that are duplicates of previous events
setopt hist_ignore_space      # remove command line from history list when first character on the line is a space
setopt hist_find_no_dups      # When searching history don't display results already cycled through twice
setopt hist_reduce_blanks     # Remove extra blanks from each command line being added to history
setopt hist_verify            # don't execute, just expand history
setopt share_history          # imports new commands and appends typed commands to history
setopt always_to_end          # When completing from the middle of a word, move the cursor to the end of the word
setopt auto_menu              # show completion menu on successive tab press. needs unsetop menu_complete to work
setopt auto_name_dirs         # any parameter that is set to the absolute name of a directory immediately
# setopt correct                # spelling correction for commands
# setopt correctall             # spelling correction for arguments

# from: https://thevaluable.dev/zsh-install-configure/
setopt AUTO_PUSHD             # Push the current directory visited on the stack.
setopt PUSHD_IGNORE_DUPS      # Do not store duplicates in the stack.
setopt PUSHD_SILENT           # Do not print the directory stack after pushd or popd.
alias s='dirs -v'
for index ({1..9}) alias "$index"="cd +${index}"; unset index

setopt auto_pushd

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/karlo/.local/lib/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/karlo/.local/lib/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/home/karlo/.local/lib/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/karlo/.local/lib/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

# setup complete for kitty
# kitty + complete setup zsh | source /dev/stdin

# Load aliases and shortcuts if existent.
# [ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shortcutrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shortcutrc"
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/aliasrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/aliasrc"
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/functionrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/functionrc"
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/fzf/fzf-conf" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/fzf/fzf-conf"

# To customize prompt, run `p10k configure` or edit ~/.config/zsh/.p10k.zsh.
[[ ! -f "${XDG_CONFIG_HOME:-$HOME/.config}/zsh/.p10k.zsh" ]] || source ${XDG_CONFIG_HOME:-$HOME/.config}/zsh/.p10k.zsh

# keybindings
# vi mode
bindkey -v
export KEYTIMEOUT=1

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -M vicmd v edit-command-line
bindkey -v '^?' backward-delete-char

bindkey '^[[P' delete-char

# bindkey -s '^z' 'zl_search\n'
bindkey -s '^p' 'on\n'
bindkey -s '^t' 'tat\n'
# bindkey -s '^g' 'rg-fzf\n'
bindkey -s '^n' 'cd_with_fzf\n'

# Make the `beginning/end` of line and `bck-i-search` commands work within tmux
# bindkey '^R' history-incremental-search-backward
# https://dwmkerr.com/effective-shell-part-1-navigating-the-command-line/
# see: ~/.config/bash/bash_commands.txt
bindkey -s '^R' 'fh\n'
bindkey -s '^Z' 'rg-fzf\n'
bindkey '^@' set-mark-command
bindkey '^A' beginning-of-line
bindkey '^B' backward-char
bindkey '^D' delete-char-or-list
bindkey '^E' end-of-line
bindkey '^F' forward-char
bindkey '^H' backward-kill-word
bindkey '^I' expand-or-complete # equals Tab
bindkey '^L' clear-screen

# zoxide
# eval "$(zoxide init zsh)"

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

## NOTE: this is for vterm in Emacs
vterm_printf(){
    if [ -n "$TMUX" ] && ([ "${TERM%%-*}" = "tmux" ] || [ "${TERM%%-*}" = "screen" ] ); then
        # Tell tmux to pass the escape sequences through
        printf "\ePtmux;\e\e]%s\007\e\\" "$1"
    elif [ "${TERM%%-*}" = "screen" ]; then
        # GNU screen (screen, screen-256color, screen-256color-bce)
        printf "\eP\e]%s\007\e\\" "$1"
    else
        printf "\e]%s\e\\" "$1"
    fi
}


# installed, install it on the machine.
# ZPLUG_HOME="${XDG_LIB_HOME:-$HOME/.local/lib}/zsh/zplug"
if [ ! -d $ZPLUG_HOME ]; then
  echo "Installing 'zplug'..."
  git clone https://github.com/zplug/zplug $ZPLUG_HOME --depth=1 --branch master
  # chown -R ${USER}:${GROUP} ${ZPLUG_HOME}
  # chmod -R o+x,g+x ${ZPLUG_HOME}
fi

# Plugins area
[ -f "$ZPLUG_HOME/init.zsh" ] && source "$ZPLUG_HOME/init.zsh"

# Let zplug manage zplug
# To manage zplug itself like other packages
zplug 'zplug/zplug', hook-build:'zplug --self-manage'

# easy fzf navigation
zplug "changyuheng/fz", defer:1
zplug "rupa/z", use:z.sh
FZ_CMD=j
FZ_SUBDIR_CMD=jj

# powerlevel10k theme
zplug "romkatv/powerlevel10k", as:theme, depth:1

# fzf completion on tab press
zplug "Aloxaf/fzf-tab"

# command auto-suggestion based on history https://coderwall.com/p/pb1uzq/z-shell-colors
zplug "zsh-users/zsh-autosuggestions"
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=8"

# interactive cd
zplug "changyuheng/zsh-interactive-cd"

# much better than zsh-syntax-highlighting
# and no conflict with autosuggestions
zplug "zdharma-continuum/fast-syntax-highlighting"

# manage dotfiles with bare repos
zplug "kazhala/dotbare"

# completions
zplug "zsh-users/zsh-completions"

# zplug "sobolevn/wakatime-zsh-plugin"
# zplug "jeffreytse/zsh-vi-mode"

zplug "jimhester/per-directory-history"
PER_DIRECTORY_HISTORY_TOGGLE=^O
HISTORY_START_WITH_GLOBAL=false
HISTORY_BASE="$HOME/.local/share/shell-history/per-directory-history"
## NOTE: setting the histfile below redirect the history to the global histoy file defined in ~/.profile
# HISTIFILE="$HOME/.local/share/shell-history/zsh_history"

# Finally: then, source plugins and add commands to $PATH
# zplug clean
# zplug check || zplug install
zplug load

## starship prompt
# eval "$(starship init zsh)"
