#!/usr/bin/env bash

EXT="Extensions/keyboardShortcut.js"

if [ ! -f "${EXT}" ]
then
    wget -O "${EXT}" https://raw.githubusercontent.com/khanhas/spicetify-cli/master/Extensions/keyboardShortcut.js
else
    echo "${EXT}" exists!
fi
