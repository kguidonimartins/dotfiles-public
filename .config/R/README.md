# Speeding up packages installation in R

Here, I'm using two strategies for speeding up packages installation.

## Using parallel processes

Original post [here][1].

First, detect how many cores are available in your machine. You may need to install the `{parallel}`.

```r
parallel::detectCores()
```

Then, in your `.Rprofile`, pass the number of available cores in the `Ncpus` argument of the `options()` function.

```r
options(Ncpus = 4)
```

## Caching

Original post [here][2]

This strategy depends on [ccache][3]. Make sure you have this installed in your machine before. The following content needs to be in the `~/.R/Makevars` file, which will set variables for the compilation of the packages.

```r
VER=
CCACHE=ccache
CC=$(CCACHE) gcc$(VER)
CXX=$(CCACHE) g++$(VER)
CXX11=$(CCACHE) g++$(VER)
CXX14=$(CCACHE) g++$(VER)
FC=$(CCACHE) gfortran$(VER)
F77=$(CCACHE) gfortran$(VER)
```

And, the cache is configured in the `~/.ccache/ccache.conf` file, which the following content:

```r
max_size = 5.0G
# important for R CMD INSTALL *.tar.gz as tarballs are expanded freshly -> fresh ctime
sloppiness = include_file_ctime
# also important as the (temp.) directory name will differ
hash_dir = false
```

[1]: https://www.jumpingrivers.com/blog/speeding-up-package-installation/

[2]: http://dirk.eddelbuettel.com/blog/2017/11/27/#011_faster_package_installation_one

[3]: https://ccache.dev/
