# Creating a system-wide navigation

This a is a little reminder about how I implement a system-wide navigation in
Arch Linux, using `xset`, `xcape`, and `xmodmap`. See the file:
[remaps](vfile:~/dotfiles/bin/remaps)

Now, I can hold space and navigate with `hjkl` as arrows keys,
and `uiop` as pgup, home, end, and pgdn, respectively.

Adapted from:

[1]: https://unix.stackexchange.com/a/434143
[2]: https://github.com/alols/xcape/blob/master/README.md
