# Git configs

## [lazygit](https://github.com/jesseduffield/lazygit):

Installation:

```bash
go get github.com/jesseduffield/lazygit
```

Check features here: https://www.youtube.com/watch?v=CPLdltN7wgE&t=66s

## About git_template

See: 
https://monoinfinito.wordpress.com/2013/08/13/git-tip-auto-update-your-ctags/

https://tbaggery.com/2011/08/08/effortless-ctags-with-git.html


