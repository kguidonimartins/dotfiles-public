# Install jupyter extensions

Using conda, type:

```bash
conda install -c conda-forge jupyter_contrib_nbextensions
```

Remember, use conda env to made the extensions available.

```bash
conda activate base
jupyter notebook
```

Source: https://medium.com/p/75d309f972a/responses/show

