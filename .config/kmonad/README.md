# Activation

Following the instructions [here](https://github.com/kmonad/kmonad/blob/master/doc/faq.md#q-how-do-i-get-uinput-permissions). In sum, you need to check if your `$USER` is included in the group `input`, else:

```bash
sudo groupadd uinput
sudo usermod -aG uinput $USER
```

Create a new file at `/lib/udev/rules.d/60-uinput.rules` with the following content:

```bash
KERNEL=="uinput", MODE="0660", GROUP="uinput", OPTIONS+="static_node=uinput"
```

Run the following command:

```bash
sudo modprobe uinput
```

`kmonad` needs access input events. So to run commands without `sudo`, for example those in your `~/.xinitrc`, run:

```bash
# in my case, for my keyboard
sudo chmod a+r /dev/input/event18
```

source: https://github.com/kmonad/kmonad
source: https://www.reddit.com/r/emacs/comments/oyzfz9/kmonad_and_the_power_of_infinite_leader_keys/

