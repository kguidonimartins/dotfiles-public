#!/bin/sh

## 27 as main and notebook in right
# xrandr --output eDP-1 --primary --mode 1366x768 --pos 1920x0 --rotate normal --output HDMI-1 --mode 1920x1080 --pos 0x0 --rotate normal --dpi 96

## 27 as main and notebook in left
xrandr --output eDP-1 --primary --mode 1366x768 --pos 0x0 --rotate normal --output HDMI-1 --mode 1920x1080 --pos 1366x0 --rotate normal --dpi 96


## notebook as main and monitor in left with left orientation
# xrandr --output eDP-1 --mode 1366x768 --pos 1080x210 --rotate normal --output HDMI-1 --primary --mode 1920x1080 --pos 0x0 --rotate left --dpi 96

## dell 27 on vertical position on the left, samsung 23 in the middle and the notebook on the right
# xrandr --output eDP-1 --primary --mode 1366x768 --pos 3000x241 --rotate normal --output HDMI-1 --mode 1920x1080 --pos 1080x241 --rotate normal --output DVI-I-2-1 --mode 1920x1080 --pos 0x0 --rotate left --dpi 96
