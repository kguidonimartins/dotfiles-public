static const char *colors[SchemeLast][2] = {
/*                       fg         bg       */
	[SchemeNorm] = { "#bbbbbb", "#132738" },
	[SchemeSel]  = { "#eeeeee", "#005577" },
};
