//Modify this file to change what commands output to your statusbar, and recompile using the make command.

// config location:
// ~/.local/suckless/dwmblocks/config.h

/* Backup */
  /* {"" , "~/.local/bin/tools/statusbar/music"             , 5       , 10 } , */
  /* {"" , "~/.local/bin/tools/statusbar/weather"           , 600     , 13 } , */
  /* {"" , "~/.local/bin/tools/statusbar/activity-watch"    , 5       , 19 } , */
  /* {"" , "~/.local/bin/tools/statusbar/microphone"        , 2       , 13 } , */
  /* {"" , "~/.local/bin/tools/statusbar/battery"           , 10      , 3  } , */
  /* {"" , "~/.local/bin/tools/statusbar/notify_dropbox"    , 6       , 5  } , */
  /* {""   , "~/.local/bin/tools/statusbar/clock"              , 5        , 0  } , */
  /* {""   , "cat /tmp/recordingicon 2>/dev/null"              , 0        , 9  } , */
  /* {""   , "~/.local/bin/tools/statusbar/counttmuxsessions"  , 5        , 6  } , */
  /* {""   , "~/.local/bin/tools/statusbar/disk"               , 5        , 20 } , */
/* {""   , "~/.local/bin/tools/statusbar/rsync_status"       , 5        , 11 } , */
/* {""   , "~/.local/bin/tools/statusbar/scp_status"         , 5        , 10 } , */

static const Block blocks[] = {
/*       Icon , Command                                           , Interval , Signal */
        {""   , "~/.local/bin/tools/statusbar/mpris_status"       , 5        , 11 } ,
        {""   , "~/.local/bin/tools/statusbar/notify_googledrive" , 150      , 3  } ,
        {"" , "~/.local/bin/tools/statusbar/notify_emacs"         , 5        , 18 } ,
        {""   , "~/.local/bin/tools/statusbar/counttmuxsessions"  , 5        , 6  } ,
        {""   , "~/.local/bin/tools/statusbar/countrsessions"     , 5        , 5  } ,
        {""   , "~/.local/bin/tools/statusbar/cpu_memory"         , 5        , 17 } ,
        {""   , "~/.local/bin/tools/statusbar/pacpackages"        , 0        , 8  } ,
        {""   , "~/.local/bin/tools/statusbar/mailbox"            , 5        , 7  } ,
        {""   , "~/.local/bin/tools/statusbar/volume"             , 3        , 12 } ,
        {""   , "~/.local/bin/tools/statusbar/internet"           , 10       , 4  } ,
        {""   , "~/.local/bin/tools/statusbar/calendar"           , 15       , 1  } ,
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char *delim = "  ";
/* static unsigned int delimLen = 1; */

// autocmd BufWritePost ~/.config/suckless/dwmblocks/config.h !cd ~/.config/suckless/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }
