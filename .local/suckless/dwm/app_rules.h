// -*- compile-command: "sudo make clean install" -*-

static const Rule rules[] = {
    /* xprop(1):
     *  WM_CLASS(STRING) = instance, class
     *  WM_NAME(STRING) = title
     */
    /* 1 == 1 << 0 */
    /* 2 == 1 << 1 */
    /* 3 == 1 << 2 */
    /* 4 == 1 << 3 */
    /* 5 == 1 << 4 */
    /* 6 == 1 << 5 */
    /* 7 == 1 << 6 */
    /* 8 == 1 << 7 */
    /* 9 == 1 << 8 */
        /* class           instance      title       tags mask    isfloating   isterminal  noswallow  monitor */
        /* just send to any tag in monitor 0 */
        { "Insync",        NULL,         NULL,       0,         1,           0,         0,         -1 },
        { "copyq",         NULL,         NULL,       0,         1,           0,         0,         -1 },
        /* just send to any tag in monitor 1 */
        /* tag 2 */
        { "Nautilus",      NULL,         NULL,         1<<1,       0,           0,         0,         -1 },
        { "Nemo",          NULL,         NULL,         1<<1,       0,           0,         0,         -1 },
        /* tag 3 */
        { "Brave",         NULL,         NULL,       1 << 2,       0,           0,         0,         -1 },
        { "firefox",       NULL,         NULL,       1 << 2,       0,           0,         0,          2 },
        /* tag 4 */
        { "qutebrowser",   NULL,         NULL,       1 << 3,       0,           0,         0,          0 },
        { "Toggl Desktop", NULL,         NULL,       1 << 3,       0,           0,         0,          0 },
        { "RStudio",       NULL,         NULL,       1 << 3,       0,           0,         0,         -1 },
        /* tag 5 */
        { "Soffice",       NULL,         NULL,       1 << 4,       0,           0,         0,         -1 },
        { "jetbrains-idea-ce",       NULL,         NULL,       1 << 4,       0,           0,         0,         -1 },
        /* tag 6 */
        { "Google-chrome", NULL,         NULL,       1 << 5,       0,           0,         0,         -1 },
        { "zoom",          NULL,         NULL,       1 << 5,       0,           0,         0,          0 },
        { "Code",          NULL,         NULL,       1 << 5,       0,           0,         0,         -1 },
        { "Gimp",          NULL,         NULL,       1 << 5,       0,           0,         0,          0 },
        /* { "Zathura",       NULL,         NULL,       1 << 5,       0,           0,         0,          1 }, */
        /* { "okular",        NULL,         NULL,       1 << 5,       0,           0,         0,          1 }, */
        { "CherryTomato",  NULL,         NULL,       1 << 5,       1,           0,         0,          0 },
        /* tag 7 */
        { "R_x11",         NULL,         NULL,       1 << 6,       0,           0,         0,          0 },
        { "Zotero",        NULL,         NULL,       1 << 6,       0,           0,         0,         -1 },
        { "mpv",           NULL,         NULL,       1 << 6,       0,           0,         0,         -1 },
        { "Remmina",       NULL,         NULL,       1 << 6,       0,           0,         0,         -1 },
        { "Vivaldi-stable", NULL,         NULL,       1 << 6,       0,           0,         0,         -1 },
        { NULL,            NULL,         "Data: .",  1 << 6,       0,           0,         0,          0 },
        /* tag 8 */
        { "QGIS3",         NULL,         NULL,       1 << 7,       0,           0,         0,         -1 },
        { "pulseeffects",  NULL,         NULL,       1 << 7,       0,           0,         0,          0 },
        /* tag 9 */
        { "Emacs",         NULL,         NULL,       1 << 8,       0,           0,         0,         -1 },
        { NULL,            NULL, "misc::view_vd",    1 << 8,       0,           0,         0,         -1 },
        { NULL,           NULL,"Special REPL",       1 << 8,       0,           0,         0,          2 },
        { NULL,            NULL,"FUNK-OFFICE",       1 << 8,       0,           0,         0,          0 },
        { NULL,            NULL,         "t:9",      1 << 8,       0,           0,         0,          0 },
        { "Tad",           NULL,         NULL,       1 << 8,       0,           0,         0,          0 },
        /* fly baby */
        { NULL,            NULL,"emacs-float",       0,         1,           0,         0,         -1 },
        { NULL,            "sptmux",     NULL,       SPTAG(0),     1,           1,         0,          1 },
        { NULL,            "spterm",     NULL,       SPTAG(0),     1,           1,         0,          1 },
        { NULL,            "spcalc",     NULL,       SPTAG(1),     1,           1,         0,         -1 },

};
