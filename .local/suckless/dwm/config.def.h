// -*- compile-command: "sudo make clean install" -*-

/* Constants */
#define TERMINAL "st"
#define TERMCLASS "St"

/* appearance */
static const unsigned int borderpx       = 0;  /* border pixel of windows */
static const unsigned int snap           = 10; /* snap pixel */
static const unsigned int gappih         = 10; /* horiz inner gap between windows */
static const unsigned int gappiv         = 10; /* vert inner gap between windows */
static const unsigned int gappoh         = 10; /* horiz outer gap between windows and screen edge */
static const unsigned int gappov         = 10; /* vert outer gap between windows and screen edge */
static const int smartgaps               = 0;  /* 1 means no outer gap when there is only one window */
static const int swallowfloating         = 1;  /* 1 means swallow floating windows by default */
static const unsigned int systraypinning = 0;  /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2;  /* systray spacing */
static const int systraypinningfailfirst = 1;  /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static int showsystray                   = 0;  /* 0 means no systray */
static const int showbar                 = 1;  /* 0 means no bar */
static const int topbar                  = 1;  /* 0 means bottom bar */

#include "font_and_colors.h"

static const XPoint stickyicon[]    = { {0,0}, {4,0}, {4,8}, {2,6}, {0,8}, {0,0} }; /* represents the icon as an array of vertices */
static const XPoint stickyiconbb    = {4,8};	/* defines the bottom right corner of the polygon's bounding box (speeds up scaling) */

typedef struct {
    const char *name;
    const void *cmd;
} Sp;
const char *spcmd1[] = {TERMINAL, "-n", "sptmux", "-g", "120x34", "-e", "scrax", NULL };
const char *spcmd2[] = {TERMINAL, "-n", "spcalc", "-f", "monospace:size=16", "-g", "50x20", "-e", "bc", "-lq", NULL };
const char *spcmd3[] = {TERMINAL, "-n", "spterm", "-g", "120x34", NULL };
static Sp scratchpads[] = {
        /* name          cmd  */
        {"sptmux",      spcmd1},
        {"spranger",    spcmd2},
        {"spterm",      spcmd3},
};

/* tagging */
/*                              1      2      3      4      5      6      7      8      9 */
/* static const char *tags[] = { " ", " ", " ", " ", " ", " ", " ", " ", " " }; */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

/* app rules */
#include "app_rules.h"

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int attachdirection = 2;    /* 0 default, 1 above, 2 aside, 3 below, 4 bottom, 5 top */

/* #include "layouts.c" */

#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"

static const Layout layouts[] = {
    /* symbol     arrange function */
    { "[]=",      tile },    /* first entry is default */
    { "><>",      NULL },    /* no layout function means floating behavior */
    { "[M]",      monocle },
    { "H[]",      deck },
    { NULL,       NULL},
};

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-l", "10", "-m", dmenumon, "-fn", dmenufont, "-nb", col_normal_background, "-nf", col_normal_foreground, "-sb", col_selected_background, "-sf", col_normal_foreground, NULL };
static const char *termcmd[]  = { "st", NULL };

/* commands spawned when clicking statusbar, the mouse button pressed is exported as BUTTON */
static char *statuscmds[] = { "notify-send Mouse$BUTTON" };
static char *statuscmd[] = { "/bin/sh", "-c", NULL, NULL };

#include "keys.h"
