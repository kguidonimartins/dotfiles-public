static const char col_normal_background[]   = "#132738";
static const char col_normal_foreground[]   = "#FFFFFF";
static const char col_selected_background[] = "#2A265A";
static const char col_black[] = "#000000";
