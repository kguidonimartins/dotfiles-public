// -*- compile-command: "sudo make clean install" -*-

/*  NOTE 2022-06-10: Big clean up. Some key definitions live on ~/.config/sxhkd/sxhkdrc */

/* See unicode definition for linux input keyboards: =/usr/include/X11/keysymdef.h= */
#define XK_colon 0x003a /* U+003A COLON */

/* key definitions */
#define MODKEY Mod4Mask
#define ALTKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
    { MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
    { MODKEY|ALTKEY,                KEY,      viewall,        {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
    { MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },


#include <X11/XF86keysym.h>
#include "shiftview.c"

/* NOTE 06-08-2021: focusonnetactive patch and xdo */
/* https://www.reddit.com/r/suckless/comments/ox329q/how_can_i_make_dwm_automatically_switch_to_the/ */
/* xdo activate -N Gimp || gimp */

static Key keys[] = {
    /* modifier             key                 function        argument */
    { MODKEY,               XK_F6,              togglesystray,  {0} },
    { 0,                    XK_F12,             togglescratch,  {.ui = 0} },
    { MODKEY,               XK_grave,           togglescratch,  {.ui = 0} },
    TAGKEYS(                XK_1,               0)
    TAGKEYS(                XK_2,               1)
    TAGKEYS(                XK_3,               2)
    TAGKEYS(                XK_4,               3)
    TAGKEYS(                XK_5,               4)
    TAGKEYS(                XK_6,               5)
    TAGKEYS(                XK_7,               6)
    TAGKEYS(                XK_8,               7)
    TAGKEYS(                XK_9,               8)
    { MODKEY,               XK_0,               view,           {.ui = ~0 } },
    /*  NOTE 2022-06-10: Don't delete yet; it can be useful! */
    /*  NOTE: This is very similar to the `togglesticky` */
    /* { MODKEY|ShiftMask,     XK_0,               tag,            {.ui = ~0 } }, */
    { MODKEY,               XK_Tab,             goback,         {0} },
    { MODKEY,               XK_q,               killclient,     {0} },
    { MODKEY|ShiftMask,     XK_q,               quit,           {0} },
    { MODKEY|ControlMask,   XK_q,               quit,           {0} },
    { MODKEY,               XK_r,               goback,         {0} },
    { MODKEY,               XK_y,               setlayout,      {.v = &layouts[0]} }, /* tile */
    { MODKEY|ShiftMask,     XK_y,               cyclelayout,    {.i = +1} }, /* cycle layouts defined in the layout array */
    { MODKEY,               XK_u,               setlayout,      {.v = &layouts[2]} }, /* monocle */
    { MODKEY|ShiftMask,     XK_u,               togglesystray,  {0} },
    { MODKEY,               XK_o,               incnmaster,     {.i = +1 } },
    { MODKEY|ShiftMask,     XK_o,               incnmaster,     {.i = -1 } },
    { MODKEY,               XK_p,               shiftview,      {.i = -1 } },
    { MODKEY,               XK_bracketleft,     focusmaster,    {0} },

    /*  NOTE 2022-06-10: Don't delete yet; it can be useful! */
    /*  NOTE: This is very similar to `goback` */
    /* { MODKEY,               XK_backslash,       view,           {0} }, */
    { MODKEY,               XK_a,               togglegaps,     {0} },
    { MODKEY|ShiftMask,     XK_a,               defaultgaps,    {0} },
    { MODKEY,               XK_s,               togglesticky,   {0} },
    { MODKEY|ShiftMask,     XK_s,               togglescratch,  {.ui = 0} },
    { MODKEY,               XK_f,               togglefullscreen,  {0} },
    { MODKEY|ShiftMask,     XK_f,               togglefakefullscreen, {0} },
    { MODKEY,               XK_h,               focusmon,       {.i = -1 } }, // this need to be changed depending of the monitor position
    { MODKEY|ShiftMask,     XK_h,               tagmon,         {.i = -1 } }, // this need to be changed depending of the monitor position
    { MODKEY|ControlMask,   XK_h,               setmfact,       {.f = -0.05} },
    { MODKEY,               XK_j,               focusstack,     {.i = +1 } },
    { MODKEY,               XK_k,               focusstack,     {.i = -1 } },
    { MODKEY,               XK_l,               focusmon,       {.i = +1 } }, // this need to be changed depending of the monitor position
    { MODKEY|ShiftMask,     XK_l,               tagmon,         {.i = +1 } }, // this need to be changed depending of the monitor position
    { MODKEY|ControlMask,   XK_l,               setmfact,       {.f = +0.05} },
    /* { MODKEY|ShiftMask,     XK_semicolon,       goback,         { .i = 1 } }, */
    /*  NOTE 2022-06-10: Don't delete yet; it can be useful! */
    /* { MODKEY,               XK_apostrophe,      togglescratch,  {.ui = 1} }, */
    { MODKEY,               XK_Return,          spawn,          {.v = termcmd } },

    { MODKEY|ShiftMask,     XK_z,               incrgaps,       {.i = +3 } },
    { MODKEY|ControlMask,   XK_z,               incrgaps,       {.i = -3 } },
    { MODKEY|ShiftMask,     XK_v,               view,           {.ui = ~0 } },
    { MODKEY,               XK_b,               togglebar,      {0} },
    { MODKEY,               XK_n,               shiftview,      { .i = +1 } },
    { MODKEY,               XK_comma,           view_adjacent,  { .i = -1 } },
    { MODKEY,               XK_period,          view_adjacent,  { .i = +1 } },

    { MODKEY,               XK_space,           zoom,           {0} },
    { MODKEY|ShiftMask,     XK_space,           togglefloating, {0} },

};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
    /* click                event mask      button          function        argument */
    { ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
    { ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
    { ClkWinTitle,          0,              Button2,        zoom,           {0} },
    { ClkStatusText,        0,              Button1,        sigdwmblocks,   {.i = 1} },
    { ClkStatusText,        0,              Button2,        sigdwmblocks,   {.i = 2} },
    { ClkStatusText,        0,              Button3,        sigdwmblocks,   {.i = 3} },
    { ClkStatusText,        0,              Button4,        sigdwmblocks,   {.i = 4} },
    { ClkStatusText,        0,              Button5,        sigdwmblocks,   {.i = 5} },
    { ClkStatusText,        ShiftMask,      Button1,        sigdwmblocks,   {.i = 6} },
    { ClkStatusText,        ShiftMask,      Button3,        spawn,          SHCMD(TERMINAL " -e nvim ~/.local/suckless/dwmblocks/config.h") },
    { ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
    { ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
    { ClkClientWin,         MODKEY,         Button1,        resizemouse,    {0} },
    { ClkTagBar,            0,              Button1,        view,           {0} },
    { ClkTagBar,            0,              Button3,        toggleview,     {0} },
    { ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
    { ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
