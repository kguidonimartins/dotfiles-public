
static const char *fonts[]          = {
                                        /* "UbuntuMono Nerd Font:size=10:antialias=true:autohint=12", */
                                        "JetBrains Mono Nerd Font:size=8.5:antialias=true:autohint=12",
                                        "Font Awesome 5:size=8:antialias=true:autohint=true"
                                      };
static char dmenufont[]             = "JetBrains Mono Nerd Font:size=8:antialias=true:autohint=12" ;

#include "auto-colors.h"

static const char *colors[][3]      = {
  /*                   fg         bg         border   */
  [SchemeSel]      = { col_normal_foreground, col_normal_background,   col_black  }, // border of the selected window
  [SchemeNorm]     = { col_normal_foreground, col_normal_background,   col_black  }, // border of the unselected window
  [SchemeTagsSel]  = { col_normal_foreground, col_selected_background, col_black  }, // Tagbar left selected {text,background,not used but cannot be empty}
  [SchemeTagsNorm] = { col_normal_foreground, col_normal_background,   col_black  }, // Tagbar left unselected {text,background,not used but cannot be empty}
  [SchemeStatus]   = { col_normal_foreground, col_normal_background,   col_black  }, // Statusbar right {text,background,not used but cannot be empty}
  [SchemeInfoSel]  = { col_normal_foreground, col_normal_background,   col_black  }, // infobar middle  selected {text,background,not used but cannot be empty}
  [SchemeInfoNorm] = { col_normal_foreground, col_normal_background,   col_black  }, // infobar middle  unselected {text,background,not used but cannot be empty}
};
