{:user {:local-repo #=(eval (str (System/getenv "XDG_CACHE_HOME") "/m2"))
        :repositories  {"local" {:url #=(eval (str "file://" (System/getenv "XDG_DATA_HOME") "/m2"))
                                 :releases {:checksum :ignore}}}
        :plugins [[refactor-nrepl "2.5.0"]
                  [cider/cider-nrepl "0.28.1"]]
        }}
