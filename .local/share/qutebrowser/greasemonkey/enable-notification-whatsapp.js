// ==UserScript==
// @name        Fix web WhatsApp Notifications
// @description	Hits the notification button, once
// @version		1.0
// @namespace   https://web.whatsapp.com/
// @run-at document-idle
// @grant       none
// ==/UserScript==
(function() {
    'use strict';

    var waitForThatFrickingButton = setInterval(function() {
        // NOTE 2022-05-25:
        // You need to get the entire class string before "Turn on desktop notifications"
        // For example: <span class="edeob0r2 t94efhq2">Turn on desktop notifications</span>
        // see: https://github.com/qutebrowser/qutebrowser/issues/5433#issuecomment-869864679
        // see: https://github.com/qutebrowser/qutebrowser/issues/5233#issuecomment-614444694
        // see: https://github.com/qutebrowser/qutebrowser/issues/5233#issuecomment-869870310
        let xpath = "//span[@class='edeob0r2 t94efhq2'][contains(.,'Turn on desktop notifications')]";
        let button = document.evaluate(xpath, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
        if(button) {
            button.click();
            clearInterval(waitForThatFrickingButton);
        }
    }, 500)
})();
