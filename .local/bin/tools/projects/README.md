# projects

These are my tools designed to create a full project structure for consulting. Most of my work use R, so part of these tools are R specific. A part of these, the other tools can be used alone when needed. See [cookiecutter][1].

The main tool here is the `init-proj` which run all the others tools sequentially.

## Structure

<!--START_SECTION:treeinfo-->
```
./
|-- ./addcodetemplate*
|    [ ## Add a minimal structure for coding in R
|-- ./adddirs*
|    [ ## Create a minimal folder structure
|-- ./addeditorconfig*
|    [ ## Add .editorconfig file because it matters
|-- ./addgit*
|    [ ## Create a git repository if not present
|-- ./addgithubrepo*
|    [ ## Create and send the project structure to a private repo
|-- ./addgitignore*
|    [ ## Fetch a gitignore using the toptal api
|-- ./addinfo*
|    [ ## Add a .info file used to generate this tree; only the files listed here will be showed in the tree view
|-- ./addmakefile*
|    [ ## Add a minimal Makefile
|-- ./addreadme*
|    [ ## Add a minimal README file and a todolist inside of it
|-- ./gettreeinfo*
|    [ ## Internal tool to generate this tree view
|-- ./init-proj*
|    [ ## Run all tools
`-- ./inserttreeinfo*
     [ ## Internal tool to generate this tree view
```

<!--END_SECTION:treeinfo-->

[1]: https://drivendata.github.io/cookiecutter-data-science/
