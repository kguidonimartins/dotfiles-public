#!/bin/sh

# This scripts generate the `*.sh` scripts used internally by tmuxinator.
# You can check how these scripts are generated using `tmuxinator debug my_project`.

DESTINATION="yml_as_script"

# for yml in `fd .yml\$`
# do
#     tmuxinator debug "$(basename ${yml%.*})" > "$DESTINATION/$(basename ${yml%.*}).sh"
#     echo Saving "$DESTINATION/$(basename ${yml%.*}).sh"
# done

for yml in `tmuxinator list | tail -n +2`
do
    tmuxinator debug "$yml" > "$DESTINATION/$yml.sh"
    echo Saving "$DESTINATION/$yml.sh"
done

