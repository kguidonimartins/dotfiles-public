#!/bin/sh

for yml in `fd .yml ~/.config/tmuxinator`
do

    filepath=`grep "root:" $yml | \
        sed 's/root\: ~/\/home\/karlo/g' | \
        awk '{print $1"/.tmuxinator.yml"}'`

            if [ -d "$(dirname $filepath)" ]
            then
                touch "$filepath"
                cat "$yml" > "$filepath"
                echo "Check $filepath"
            fi

done

