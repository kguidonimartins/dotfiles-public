# tools

Binaries location in my system. Because `python` install binaries inside
`~/.local/bin` by default, I decided to organize my `$PATH` in this way.
The very first command (see below) in the `~/.profile` is responsible to put all of
these subdirectories in my `$PATH`.

```bash
# put everything in the PATH
export PATH="$PATH:$(du "$HOME/.local/bin/" | cut -f2 | paste -sd ':')"
```
