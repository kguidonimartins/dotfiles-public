#!/usr/bin/env bash

set -e

usage() {
cat <<EOF
Create a bibtex file from note files with bib header, e.g.:

~~~.bib
@article{Zilla_2020,
...
}
~~~

Usage: $(basename "$0") <[options]>

Options:
        -p    path to the note folder
        -b    path to the global bib file

Examples:
        note2bib -p bibliography/notes -b bibliography/bibliography.bib
EOF
exit 1
}

while getopts ":p:b:" o; do
    case "${o}" in
        p )
            p=${OPTARG}
            ;;
        b )
            b=${OPTARG}
            ;;
        * )
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${p}" ] || [ -z "${b}" ]; then
    usage
fi

if [ ! -d "${p}" ]; then
    echo "[ERROR] '${p}' path does not exists!"
    exit 1
fi

if [ ! -f "${b}" ]; then
    echo "[ERROR] '${b}' path does not exists!"
    exit 1
fi

[[ -f "${b}" ]] && rm "${b}"
pcregrep -r -h -M -I --exclude-dir=img --exclude=".*pdf" --exclude="tags" "^@(\n|.)*~~~" "${p}" | sed 's/~~~//g' > "${b}"
