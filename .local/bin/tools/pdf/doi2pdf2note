#!/usr/bin/env bash

usage() {
cat <<EOF
Get pdf from sci-hub based on doi.

Usage: $(basename "$0") <[options]>

Options:
        -d    doi
        -p    path to save the pdf file (can be empty)

Examples:
        $(basename "$0") -d 10.1016/j.tplants.2011.04.002
        $(basename "$0") -d 10.1016/j.tplants.2011.04.002 -p bibliography/articles
EOF
exit 1
}

while getopts ":d:p:" o; do
    case "${o}" in
        d )
            d=${OPTARG}
            ;;
        p )
            p=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${d}" ] || [ -z "${p}" ]; then
    usage
fi

if [ ! -d "${p}" ]; then
    echo "[ERROR] '${p}' path does not exists!"
    exit 1
fi

echo "Checking DOI first"

CHECKDOI=$(doi2bib "${d}")

if [ -z "${CHECKDOI}" ]; then
        echo "Fail to get the bib header from DOI"
        echo "Exiting"
        exit 1
fi

echo "${CHECKDOI}"

# raw_url=
# i=

# while [[ $i -lt 10 && "$raw_url" != *".pdf"* ]]; do
#     raw_url=$(curl -s https://sci-hub.se/"$d" | \
#               grep -o "location.href*.*.pdf" | \
#               sed "s/location.href='//g")
#     i=$(( i + 1 ))
# done

# if [[ $raw_url == *"https"* ]]; then
#     pdfurl="$raw_url"
# else
#     pdfurl="https:""$raw_url"
# fi

# check_file() {
#     if [ -f "$1" ]; then
#         echo "File already exists!"
#         exit 1
#     fi
# }

# log() {
#     doi=$1
#     pdf=$2
#     echo "$doi $pdf" >> log.txt
# }

# scihub "${d}"

# if [ -z "${p}" ]; then
#     check_file "$(basename "$pdfurl")"
#     log "$d" "$(basename "$pdfurl")"
#     echo -e "Downloading as: $(basename "$pdfurl") \n"
#     curl -O "$pdfurl"
#     pdf2note -f "$(basename $pdfulr)" -p bibliography/notes -d "${d}"
# else
#     pdfname=$(basename "$pdfurl")
#     fullpath="${p}"/"$pdfname"
#     check_file "$fullpath"
#     log "$d" "$fullpath"
#     echo -e "Downloading as: $fullpath \n"
#     curl "$pdfurl" -o "$fullpath"
#     pdf2note -f "$fullpath" -p bibliography/notes -d "${d}"
# fi

# echo "Updating tags"
# notes2tags -p bibliography/notes -t tags
# echo "Updating bibliography.bib"
# notes2bib -p bibliography/notes -b bibliography/bibliography.bib

raw_url=
i=

while [[ $i -lt 10 && "$raw_url" != *".pdf"* ]]; do
    raw_url=$(curl -H 'Cache-Control: no-cache' --max-time 180 -s https://sci-hub.se/"$d" | \
              grep -o "location.href*.*.pdf" | \
              sed "s/location.href='//g")
    i=$(( i + 1 ))
    echo "[TRY $i] Getting the url for the PDF at sci-hub."
done

if [[ $raw_url == *"https"* ]]; then
    pdfurl="$raw_url"
else
    pdfurl="https:""$raw_url"
fi

check_doi_pdf_in_log() {
    if grep "$1 $2" log.txt ; then
      echo "[SKIP] '$1 $2' in the log.txt"
      exit 1
    fi
}

check_file() {
    if echo $1 | grep https; then
            echo "[ERROR] Something goes wrong."
            exit 1
    fi
}

append_letter() {
    printf $1 | sed 's#\.pdf##g' | xargs printf "%sr.pdf"
}

log() {
    doi=$1
    pdf=$2
    echo "$doi $pdf" >> log.txt
}

if [ -z "${p}" ]; then
    pdfname="$(basename "$pdfurl")"
    check_doi_pdf_in_log "$d" "$pdfname"
    # check_file "$pdfname"
    if [ -f "$pdfname" ]; then
        pdfname=$(append_letter "$pdfname")
    fi
    echo "Checking again because renaming!"
    check_doi_pdf_in_log "$d" "$pdfname"
    check_file "$pdfname"
    echo -e "Downloading as: $pdfname \n"
    curl -H 'Cache-Control: no-cache' --max-time 180 "$pdfurl" -o "$pdfname"
    if [ ! -f "$pdfname" ]; then
        echo "Trying the download again............"
        curl -H 'Cache-Control: no-cache' --max-time 180 "$pdfurl" -o "$pdfname"
    fi
    pdf2note -f "$pdfname" -p bibliography/notes -d "${d}"
    log "$d" "$pdfname"
else
    pdfname=$(basename "$pdfurl")
    fullpath="${p}"/"$pdfname"
    check_doi_pdf_in_log "$d" "$fullpath"
    if [ -f "$fullpath" ]; then
        fullpath=$(append_letter "$fullpath")
    fi
    echo "Checking again because renaming!"
    # check_file "$fullpath"
    check_doi_pdf_in_log "$d" "$fullpath"
    check_file "$pdfname"
    echo -e "Downloading as: $fullpath \n"
    curl -H 'Cache-Control: no-cache' --max-time 180 "$pdfurl" -o "$fullpath"
    if [ ! -f "$fullpath" ]; then
        echo "Trying the download again............"
        curl -H 'Cache-Control: no-cache' --max-time 180 "$pdfurl" -o "$fullpath"
    fi
    pdf2note -f "$fullpath" -p bibliography/notes -d "${d}"
    log "$d" "$fullpath"
fi
