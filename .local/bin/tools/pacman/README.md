---
title: "Tools for cleaning up pacman and yay caches"
categories: post
date: 2020-06-06
tags:
  - arch linux
  - pacman
  - yay
  - cache
description: "Automate cache clean up with pacman hooks"
---

There are several links discuting how to properly cleaning up the both pacman and yay (pacman helper) caches:

https://github.com/Jguer/yay/issues/808

https://github.com/Jguer/yay/issues/1129

https://github.com/Jguer/yay/issues/772

https://github.com/Jguer/yay/issues/178

I did adpot the below one:

https://gist.github.com/luukvbaal/2c697b5e068471ee989bff8a56507142

Pacman hooks lives by default in `/etc/pacman.d/hooks`. It can be symlinked, but remember: always use full paths to access the scripts!

## Solving problems

* Problem #1

`warning: some_package: local (1.15.3-2) is newer than core (1.15.3-1)`

** [Solution](https://bbs.archlinux.org/viewtopic.php?id=128502)

Try:

```bash
update_mirrolist
sudo pacman-db-upgrade
sudo pacman -Syuu
sudo pacman -Syu
```


