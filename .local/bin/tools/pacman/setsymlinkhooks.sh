#!/usr/bin/env bash

for hook in hook-config/*; do
    ln -vsf "$(pwd)/$hook" "/etc/pacman.d/hooks/$(basename "$hook")"
done
