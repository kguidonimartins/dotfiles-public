#!/usr/bin/env bash

# This script is called on startup to remap keys.
# Increase key speed via a rate change
xset r rate 300 50
# Map the caps lock key to super...
setxkbmap -option caps:super
# This is a little hack to implement `hjkl` as a
# system navigation:
# Map space to an unused keycode (to keep it around for xcape to use).
# Check your unused keycode with `xmodmap -pke`.
xmodmap -e "keycode 247 = space"
# Map space key to a modifier key
xmodmap -e "keycode 65 = Mode_switch"
# Implement system navigation (these need to be used with the modifier):
xmodmap -e "keysym h = h H Left"
xmodmap -e "keysym l = l L Right"
xmodmap -e "keysym k = k K Up"
xmodmap -e "keysym j = j J Down"
xmodmap -e "keysym u = u U Home"
xmodmap -e "keysym i = i I Prior" # PgUp
xmodmap -e "keysym o = o O Next" # PgDn
xmodmap -e "keysym p = p P End"
xmodmap -e "keysym q = q Q slash"         # /
xmodmap -e "keysym w = w W question"      # ?
# xmodmap -e "keysym e = e E quotedbl"      # " # good for abnt br
# xmodmap -e "keysym r = r R apostrophe"    # ' # good for abnt br
# xmodmap -e "keysym t = t T dead_grave"    # ` # good for abnt br
# when using a us keyboard board layout (set with: setxkbmap us),
# following combination will work as (semicolon [us] == ccedilla [br];
#                                     apostrophe [us] == dead_tilde [br]):
# space+; = dead_accute (acento agudo)
# space+shift+; = dead_grave (acento crase)
# space+apostrophe = dead_circumflex (acento til)
# space+shift+apostrophe = dead_circumflex (acento circunflexo)
xmodmap -e "keysym semicolon = semicolon colon dead_acute dead_grave"
xmodmap -e "keysym apostrophe = apostrophe quotedbl dead_tilde dead_circumflex"
# Now, you can navigate holding space and `hjkl`, and others remaped keys.
# The code below save the remaped caps and space. Now, 1) caps works as super
# when holded and as the escape when pressed only once; and 2) space works as
# a modifier when holded and as space when pressed only once.
killall xcape 2>/dev/null ; xcape -e 'Super_L=Escape;Mode_switch=space'
# Map the menu button to right super as well.
xmodmap -e 'keycode 135 = Super_R'
