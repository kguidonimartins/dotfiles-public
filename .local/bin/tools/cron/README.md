# Important Note

NOTE: Checkout the content of the file `/etc/profile.d/dbus.sh`. Comment out the `export $(dbus-launch)` command. Otherwise, you cannot use notify-send in cron.

These cronjobs have components that require information about your current display to display notifications correctly.

When you add them as cronjobs, I recommend you precede the command with commands as those below:

```
export DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus; export DISPLAY=:0; . $HOME/.zprofile;  then_command_goes_here
```

This ensures that notifications will display, xdotool commands will function and environmental variables will work as well.

# Location

First, check `crontab -l`. Create a new one if needed with `crontab -e`, edit and save. A new crontab for your username was created. The location of this file is: `/var/spool/cron`.

NOTE: this file can be temporary. Is a good practice maintaining a backup for that[0]. So:

```bash
# backup
crontab -l > my_crontab
# install
crontab my_crontab
```


# Starting service

If you are using cronie [1][2], you need to start the service after install the package [3]:

```bash
# to start once
systemctl start cronie
# to start at boot
systemctl enable cronie
```

# Check if there are jobs running [4]

```bash
pgrep cron
```

# Checking cronie log messages in arch linux [5]

```bash
journalctl -xb -u cronie
```

# Using cronie [5]

# Crontab Guru [6]

# 5 top reasons your crontabs scripts are not working [7]



Links:
[0]: https://askubuntu.com/a/216711
[1]: https://www.archlinux.org/packages/?name=cronie
[2]: https://wiki.archlinux.org/index.php/cron#Cronie_2
[3]: https://bbs.archlinux.org/viewtopic.php?pid=1205312#p1205312
[4]: https://askubuntu.com/a/23469
[5]: https://forum.manjaro.org/t/how-to-create-a-cron-job-in-manjaro/105
[6]: https://crontab.guru/
[7]: https://blog.ndk.name/top-5-reasons-your-crontab-scripts-are-not-working/
