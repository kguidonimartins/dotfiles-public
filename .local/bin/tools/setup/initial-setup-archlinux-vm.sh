#! /bin/bash

echo "You have available tmux conf"
cat > .tmux.conf << EOF
unbind C-b
set -g prefix C-a
unbind '"'
bind-key | split-window -h  -c '#{pane_current_path}'
unbind %
bind-key - split-window -v  -c '#{pane_current_path}'
setw -g mode-keys vi
bind c new-window -c "#{pane_current_path}"
bind-key h select-pane -L
bind-key j select-pane -D
bind-key k select-pane -U
bind-key l select-pane -R
bind-key Tab select-pane -t :.+
bind-key -r C-h resize-pane -L
bind-key -r C-j resize-pane -D
bind-key -r C-k resize-pane -U
bind-key -r C-l resize-pane -R
EOF

# filesystem mounting
echo "This script will create and format the partitions as follows:"
echo "Assuming a VM with storage 30Gib and RAM 2Gib!"
echo "/dev/sda1 - 200Mib will be mounted as /efi"
echo "/dev/sda2 - 4GiB will be used as swap"
echo "/dev/sda3 - 10GiB will be used as /"
echo "/dev/sda4 - rest of space will be mounted as /home"
read -p 'Continue? [y/N]: ' fsok
if ! [ $fsok = 'y' ] && ! [ $fsok = 'Y' ]
then
  echo "Edit the script to continue..."
  exit
fi

sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | fdisk /dev/sda
  o # clear the in memory partition table
  n # new partition
  p # primary partition
  1 # partition number 1
    # <enter> default - start at beginning of disk
  +200M # 200 MiB boot parttion
  n # new partition
  p # primary partition
  2 # partion number 2
    # <enter> default, start immediately after preceding partition
  +4G # 4 GiB swap parttion
  n # new partition
  p # primary partition
  3 # partion number 3
    # <enter> default, start immediately after preceding partition
  +10G # 10 GiB / parttion
  n # new partition
  p # primary partition
    # <enter>
    # <enter> rest of space will be /home
  w # write the partition table
  q # and we're done
EOF

echo "check partition table again"
lsblk

echo "formating partitions"
echo "formating /boot"
mkfs.ext4 /dev/sda1
echo "formating /"
mkfs.ext4 /dev/sda3
echo "formating /home"
mkfs.ext4 /dev/sda4
echo "formating and activating swap"
mkswap /dev/sda2
swapon /dev/sda2

echo "check partition table again"
lsblk

echo "setting time zone"
timedatectl set-ntp true

echo "mounting partitions"
mount /dev/sda3 /mnt
lsblk
mkdir /mnt/boot
mkdir /mnt/home
ls /mnt
mount /dev/sda1 /mnt/boot
mount /dev/sda4 /mnt/home

pacman --no-confirm --needed -Sy
pacman --no-confirm --needed -Sy archlinux-keyring

echo "installing basic packages"
pacstrap /mnt base base-devel linux linux-firmware

genfstab -U /mnt >> /mnt/etc/fstab

curl https://gitlab.com/kguidonimartins/dotfiles-public/-/raw/main/.local/bin/tools/setup/chroot.sh > /mnt/chroot.sh 

echo "please run: arch-chroot /mnt bash chroot.sh && rm /mnt/chroot.sh"
echo "press any key"
read tmpvar
exit

