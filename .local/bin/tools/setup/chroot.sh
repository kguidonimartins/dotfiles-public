#!/usr/bin/env bash

ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime

echo "Enter a hostname:"
read hostname

echo $hostname > /etc/hostname
echo -e "KEYMAP=br-abnt2\nFONT=lat0–16" > /etc/vconsole.conf

cat < /etc/hosts << EOF
127.0.0.1	localhost
::1		    localhost
127.0.1.1	$hostname.localdomain	$hostname
EOF

hwclock --systohc

# locale
echo "LANG=en-US.UTF-8" > /etc/locale.conf
sed -i '/pt_BR.UTF-8 UTF-8/s/^#//g' /etc/locale.gen
sed -i '/en_US.UTF-8 UTF-8/s/^#//g' /etc/locale.gen
locale-gen

pacman --noconfirm --needed -S networkmanager
systemctl enable NetworkManager
systemctl start NetworkManager

pacman --noconfirm --needed -S grub && grub-install --target=i386-pc /dev/sda && grub-mkconfig -o /boot/grub/grub.cfg

pacman --noconfirm --needed -S dialog

# archinstall() {
# 	curl -O https://gitlab.com/kguidonimartins/dotfiles-public/-/raw/main/.local/bin/tools/setup/auto-setup.sh \
# 		&& bash auto-setup.sh ;
# }

# dialog --title "Install Arch Linux" --yesno "This install script will easily let you access Arch Linux which automatically install a full Arch Linux environment.\n\nIf you'd like to install this, select yes, otherwise select no."  15 60 && archinstall
