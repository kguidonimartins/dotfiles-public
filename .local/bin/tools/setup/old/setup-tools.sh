#!/usr/bin/env sh

# export env vars
# source "${HOME}/.profile"

# tools
pacman -S --noconfirm sudo git curl neovim zsh wget tmux gcc glibc gcc-fortran make openblas ccache which libxau libxi libxss libxtst libxcursor libxcomposite libxdamage libxfixes libxrandr libxrender mesa-libgl  alsa-lib libglvnd libxml2 imagemagick

# refresh env
# source "${HOME}/.profile"

# change shell
# chsh -s $(which zsh)

# refresh
# source "${HOME}/.profile"
