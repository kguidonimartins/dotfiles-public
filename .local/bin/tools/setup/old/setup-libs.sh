#!/usr/bin/env sh

start=$(date +%s)

# export env vars
# source "${HOME}/.profile"

# libs
pacman -S python python-setuptools python-pip sqlite mpdecimal xz tk r go rust npm nodejs ruby --noconfirm

# refresh env
# source "${HOME}/.profile"

# # yay
# git clone https://aur.archlinux.org/yay.git
# cd yay
# makepkg -si
# cd ..

# python setup
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py

# essentials
pip install pipx
pip install neovim
pipx install radian
pipx install visidata

# anaconda
wget https://repo.anaconda.com/archive/Anaconda3-2020.02-Linux-x86_64.sh -O ~/anaconda.sh
bash ~/anaconda.sh -b -p "$HOME/.local/anaconda3"

# refresh env
# source "${HOME}/.profile"

# go
echo "Installing go packages"
go get -u github.com/gokcehan/lf
go get -u github.com/junegunn/fzf
go get github.com/jesseduffield/lazygit
go get github.com/jesseduffield/lazydocker
go get github.com/vrothberg/vgrep

go get github.com/msprev/fzf-bibtex/cmd/bibtex-ls
go install github.com/msprev/fzf-bibtex/cmd/bibtex-ls
go install github.com/msprev/fzf-bibtex/cmd/bibtex-markdown
go install github.com/msprev/fzf-bibtex/cmd/bibtex-cite

# refresh env
# source "${HOME}/.profile"

# R
sudo R -e "Sys.setenv('R_LIBS_USER'='${HOME}/.local/lib/R/library/4.0', 'R_PROFILE_USER'='${HOME}/.config/r/Rprofile', 'R_MAKEVARS_USER'='${HOME}/.config/R/Makevars', 'CCACHE_CONFIGPATH'='${HOME}/.config/ccache/ccache.conf', 'CCACHE_DIR'='${HOME}/.cache/ccache'); Sys.getenv(); install.packages(pkgs = c('devtools', 'tidyverse', 'reticulate', 'pacman', 'httpuv'), repos = 'https://cloud.r-project.org', dependencies = TRUE, Ncpus = 4); devtools::install_github('jalvesaq/colorout', repos='https://cloud.r-project.org', upgrade = 'always')"


# refresh env
# source "${HOME}/.profile"

# rust
cargo install exa du-dust fd-find ripgrep ripgrep_all

# ruby
gem install tmuxinator
gem install travis --no-document

# vim plug
nvim +PlugInstall +qall

end=$(date +%s)
echo "Duration: $((($(date +%s)-$start)/60)) minutes"
