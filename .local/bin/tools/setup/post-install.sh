#!/usr/bin/env bash

cd "${HOME}" || exit 1

dotbare submodule update --recursive --remote

bash https://raw.githubusercontent.com/kguidonimartins/dotfiles-public/main/.local/bin/tools/setup/install-dotfiles-public.sh

bash https://raw.githubusercontent.com/kguidonimartins/dotfiles-public/main/.local/bin/tools/setup/install-dotfiles-private.sh

# fonts
fc-cache -f -v

# neovim plugin
installnvimplugins() {
    nvim +PlugInstall +qall +qall
}

# coc lsp
installcoc() {

    # Use package feature to install coc.nvim
    # for neovim
     mkdir -p ~/.local/share/nvim/site/pack/coc/start
     cd ~/.local/share/nvim/site/pack/coc/start
     curl --fail -L https://github.com/neoclide/coc.nvim/archive/release.tar.gz | tar xzfv -

    # Install extensions
    mkdir -p ~/.config/coc/extensions
    cd ~/.config/coc/extensions
    if [ ! -f package.json ]
    then
      echo '{"dependencies":{}}'> package.json
    fi
    # Change extension names to the extensions you need
    npm install coc-snippets coc-marketplace coc-pyls coc-python coc-r-lsp \
      --global-style --ignore-scripts --no-bin-links --no-package-lock --only=prod
}

installnvimplugins

installcoc
