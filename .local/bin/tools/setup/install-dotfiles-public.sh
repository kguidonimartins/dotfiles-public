#!/usr/bin/env bash

if [ "${USER:-}" == "root" ]; then
  echo "This script works only with normal user, it wont work with root." >&2
  echo "Please log in as normal user and try again." >&2
  exit 1
fi

if ! [ -x "$(command -v git)" ]; then
  echo 'Error: git is not installed.' >&2
  exit 1
fi

BKP="${HOME}/.dotfiles-backup"

if [ "$(pwd)" != "${HOME}" ]; then
	echo "This script needs to be execute at your $HOME!"
	echo "Changing directory to your $HOME ..."
	cd "${HOME}"
fi

echo dotfiles-public >> "${HOME}"/.gitignore
echo .dotfiles-backup >> "${HOME}"/.gitignore

/usr/bin/git clone --bare https://github.com/kguidonimartins/dotfiles-public.git "${HOME}/dotfiles-public"

dot() {
    /usr/bin/git --git-dir="${HOME}/dotfiles-public/" --work-tree="${HOME}" $@
}

mkdir -p "$BKP"
dot checkout
if [ $? = 0 ]; then
  echo "Checked out config.";
  else
    echo "Backing up pre-existing dot files.";
    dot checkout 2>&1 | \
        grep -E "\s+\." | \
        awk {'print $1'} | \
        xargs -I {} sh -c "dirname {}" | \
        xargs -I {} sh -c "mkdir -p $BKP/{}";
    dot checkout 2>&1 | \
        grep -E "\s+\." | \
        awk {'print $1'} | \
        xargs -I {} sh -c "mv {} $BKP/{}"
fi;

dot checkout -f
dot config status.showUntrackedFiles no

source "${HOME}"/.profile

echo "Checking env"
echo "$XDG_CONFIG_HOME"
echo "$XDG_DATA_HOME"
echo "$XDG_CACHE_HOME"
echo "$XDG_LIB_HOME"

# chsh -s $(which zsh)

echo "Please, log out in order for changes to take effect!"
