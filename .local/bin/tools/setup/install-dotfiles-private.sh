#!/usr/bin/env bash

if [ "${USER:-}" == "root" ]; then
  echo "This script works only with normal user, it wont work with root." >&2
  echo "Please log in as normal user and try again." >&2
  exit 1
fi

if ! [ -x "$(command -v git)" ]; then
  echo 'Error: git is not installed.' >&2
  exit 1
fi

BKP="${HOME}/.dotfiles-backup"

if [ "$(pwd)" != "${HOME}" ]; then
	echo "This script needs to be execute at your $HOME!"
	echo "Changing directory to your $HOME ..."
	cd "${HOME}"
fi

echo dotfiles-private >> "${HOME}"/.gitignore
echo .dotfiles-backup >> "${HOME}"/.gitignore

/usr/bin/git clone --bare https://github.com/kguidonimartins/dotfiles-private.git "${HOME}/dotfiles-private"

dotp() {
    /usr/bin/git --git-dir="${HOME}/dotfiles-private/" --work-tree="${HOME}" $@
}

mkdir -p "$BKP"
dotp checkout
if [ $? = 0 ]; then
  echo "Checked out config.";
  else
    echo "Backing up pre-existing dot files.";
    dotp checkout 2>&1 | \
        grep -E "\s+\." | \
        awk {'print $1'} | \
        xargs -I {} sh -c "dirname {}" | \
        xargs -I {} sh -c "mkdir -p $BKP/{}";
    dotp checkout 2>&1 | \
        grep -E "\s+\." | \
        awk {'print $1'} | \
        xargs -I {} sh -c "mv {} $BKP/{}"
fi;

dotp checkout -f
dotp config status.showUntrackedFiles no

