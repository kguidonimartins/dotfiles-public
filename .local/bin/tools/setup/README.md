## Installation:

On an Arch based distribution as root, run the following:

```
curl -L auto-setup.sh > auto-setup.sh
sh auto-setup.sh
```

## Customization:

By default, the script uses the programs [here in progs.csv](progs.csv) and installs
[my dotfiles repo here](https://gitlab.com/kguidonimartins/dotfiles-public).

### The `progs.csv` list

The script will parse the given programs list and install all given programs. Note
that the programs file must be a three column `.csv`.

The first column is a "tag" that determines how the program is installed, ""
(blank) for the main repository, `A` for via the AUR or `G` if the program is a
git repository that is meant to be `make && sudo make install`ed. `V` if it's for
the void linux distribution's xbps package manager.

## Updated instructions

1. First step

Get the first script:

```bash
curl -O https://gitlab.com/kguidonimartins/dotfiles-public/-/raw/main/.local/bin/tools/setup/initial-setup-archlinux-vm.sh
```

Change the `initial-setup-archlinux-vm.sh` file in the following section:

```bash
sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | fdisk /dev/sda
  o # clear the in memory partition table
  n # new partition
  p # primary partition
  1 # partition number 1
    # <enter> default - start at beginning of disk
  +200M # 200 MiB boot parttion
  n # new partition
  p # primary partition
  2 # partion number 2
    # <enter> default, start immediately after preceding partition
  +16G # 16 GiB swap parttion
  n # new partition
  p # primary partition
  3 # partion number 3
    # <enter> default, start immediately after preceding partition
  +50G # 50 GiB / parttion
  n # new partition
  p # primary partition
    # <enter>
    # <enter> rest of space will be /home
  w # write the partition table
  q # and we're done
EOF
```

Then, just run:

```bash
bash initial-setup-archlinux-vm.sh
```

After the initial setup, run:

```bash
arch-chroot /mnt bash chroot.sh && rm /mnt/chroot.sh
```

Finish this setup creating a password for the `root` user:

```bash
passwd
```

Now, finish the basic installation:

```bash
bash auto-setup.sh
```
