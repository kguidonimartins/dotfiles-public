#!/usr/bin/env bash

# This file runs when a DM logs you into a graphical session.
# If you use startx/xinit like a Chad, this file will also be sourced.

# Fix Gnome Apps Slow  Start due to failing services
# Add this when you include flatpak in your system
# Check out too: ~/.config/cron/README.md
# Variables exported here affect cron jobs.
dbus-update-activation-environment --systemd DBUS_SESSION_BUS_ADDRESS DISPLAY XAUTHORITY

# monitor layout
bash "${XDG_CONFIG_HOME:-$HOME/.config}/screenlayout/xrandr-config.sh"

# # music player daemon-you might prefer it as a service though
# mpd &

# run the remaps script, switching caps/esc and more; check it for more info
remaps-with-qmk &
# # because bluetooth keyboard
# (sleep 30 && remaps) &

# remaps trackball buttons
set-trackball &

# set the background with the `setbg` script
setbg &

# # xcompmgr for transparency
## see:
## https://www.reddit.com/r/suckless/comments/k3dvbz/dwm_picom_not_behaving_as_expected/ge921zq?utm_source=share&utm_medium=web2x&context=3
# xcompmgr &
picom &

# hotkeys
sxhkd &

# start the statusbar
$STATUSBAR &

# dunst for notifications
dunst &

# speed xrate up
xset r rate 300 50 &

# Remove mouse when idle
unclutter &

# # Check for when to update the mpd module
# mpd-module-update &

# # spotify server via mpd
# mopidy &

# start redsfhit
redshift-gtk &

# # up emacs server
# emacs --daemon &

# (sleep 13s && rescuetime) &
# (sleep 15s && aw-server) &
# (sleep 17s && aw-watcher-afk) &
# (sleep 19s && aw-watcher-window) &
# (sleep 20s && TogglDesktop) &

# start my clipboard manager
# copyq &

# startup bluetooth
# https://wiki.archlinux.org/title/Bluetooth#Auto_power-on_after_boot
# (sleep 15s && blueman-applet) &

# use Xresources colors/settings on startup
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}"/Xresources ] && \
    xrdb "${XDG_CONFIG_HOME:-$HOME/.config}"/Xresources &

# This is the list for lf icons:
# shellcheck source=/home/karlo/.config/lf/lf-icons-and-colors
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}"/lf/lf-icons-and-colors ] && \
    . "${XDG_CONFIG_HOME:-$HOME/.config}"/lf/lf-icons-and-colors
