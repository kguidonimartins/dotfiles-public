# Profile file. Runs on login. Environmental variables are set here.

# Adds `~/.local/bin` to $PATH
export PATH="$PATH:$(du "$HOME/.local/bin/" | cut -f2 | paste -sd ':')"

# Get default WM
export WM="dwm"

# This is a trick because $WM did not have a desktop session name, and
# my fork of wallpaper-reddit depends on this variable.
export DESKTOP_SESSION="${WM}"

# dotbare
export DOTBARE_DIR="$HOME/dotfiles-public"
export DOTBARE_TREE="$HOME"

# bash_profile
# [[ -f "${XDG_CONFIG_HOME}/bash/bashrc" ]] && . "${XDG_CONFIG_HOME}/bash/bashrc"

# Default programs:
export EDITOR="nvim"
export TERMINAL="st"
export BROWSER="firefox"
export READER="zathura"
export STATUSBAR="${WM}blocks"

# Export XDG environmental variables from '~/.config/user-dirs.dirs'
eval "$(sed 's/^[^#].*/export &/g;t;d' ~/.config/user-dirs.dirs)"

# Clean up my home
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_LIB_HOME="$HOME/.local/lib"

# Libs
export PLUG_HOME="${XDG_LIB_HOME:-$HOME/.local/lib}/nvim/plug"
export ZPLUG_HOME="${XDG_LIB_HOME:-$HOME/.local/lib}/zsh/zplug"
export CARGO_HOME="${XDG_LIB_HOME:-$HOME/.local/lib}/cargo"
export GOPATH="${XDG_LIB_HOME:-$HOME/.local/lib}/go"
# export R_LIBS_USER="${XDG_LIB_HOME}/R/library/4.1"
export GEM_HOME="${XDG_LIB_HOME}/gem"
export PATH=$PATH:$XDG_LIB_HOME/gem/bin
export NPM_CONFIG_USERCONFIG="${XDG_CONFIG_HOME}/npm/npmrc"
export ASDF_DATA_DIR="${XDG_LIB_HOME:-$HOME/.local/lib}/asdf"

# paths
export PATH=$PATH:$GOPATH/bin
export PATH=$PATH:/home/karlo/.local/lib/npm/node_modules/.bin
export PATH=$PATH:/home/karlo/.local/lib/npm/bin
# export PATH=$PATH:$CARGO_HOME/bin
export PATH=$PATH:/home/karlo/.TinyTeX/bin

export GEM_SPEC_CACHE="${XDG_CACHE_HOME}/gem"

# My custom paths
export GOOGLE_DRIVE_HOME="$HOME/google-drive/kguidonimartins"
export CALIBRE_LIBRARY_HOME="$GOOGLE_DRIVE_HOME/calibre-library"
export ZOTERO_LIBRARY_HOME="$GOOGLE_DRIVE_HOME/Zotero_Library"

export DOCKER_CONFIG="$XDG_CONFIG_HOME"/docker
export SUBVERSION_HOME="${XDG_CONFIG_HOME}/subversion"
#export XAUTHORITY="$XDG_RUNTIME_DIR/Xauthority" # This line will break some DMs.
export ALSA_CONFIG_PATH="$XDG_CONFIG_HOME/alsa/asoundrc"
export BAT_CONFIG_PATH="$HOME/.config/bat/bat.conf"
export GIT_CONFIG="${XDG_CONFIG_HOME:-$HOME/.config}/git/config"
export ATOM_HOME="$XDG_CONFIG_HOME"/atom
# export CTAGS="${XDG_CONFIG_HOME:-$HOME/.config}/ctags/*.ctags"
export WAKATIME_HOME="${XDG_CONFIG_HOME:-$HOME/.config}/wakatime"
export _Z_DATA="$XDG_DATA_HOME/z"

export NOTMUCH_CONFIG="${XDG_CONFIG_HOME:-$HOME/.config}/notmuch-config"
export GTK2_RC_FILES="${XDG_CONFIG_HOME:-$HOME/.config}/gtk-2.0/gtkrc-2.0"
export LESSHISTFILE="-"
export WGETRC="${XDG_CONFIG_HOME:-$HOME/.config}/wget/wgetrc"
export INPUTRC="${XDG_CONFIG_HOME:-$HOME/.config}/inputrc"

export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"
export HISTFILE="${XDG_DATA_HOME}/shell-history/zsh_history"
export HIST_STAMPS="dd/mm/yyyy"
export HISTORY_IGNORE='(cd *|ls *|rm *|q|m|d|p|on|..|cd|nomacs *)'
export HISTSIZE=500000
export SAVEHIST=500000

export ASDF_CONFIG_FILE="${XDG_CONFIG_HOME}/asdf/asdfrc"
export ASDF_DEFAULT_TOOL_VERSIONS_FILENAME="${XDG_CONFIG_HOME}/asdf/tool-versions"

export AWS_SHARED_CREDENTIALS_FILE="$XDG_CONFIG_HOME"/aws/credentials
export AWS_CONFIG_FILE="$XDG_CONFIG_HOME"/aws/config
export VSCODE_PORTABLE="$XDG_DATA_HOME"/vscode
export SSB_HOME="$XDG_DATA_HOME"/zoom
export PARALLEL_HOME="$XDG_CONFIG_HOME"/parallel
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup
export TERMINFO="$XDG_DATA_HOME"/terminfo
export TERMINFO_DIRS="$XDG_DATA_HOME"/terminfo:/usr/share/terminfo

export CONDARC="${XDG_CONFIG_HOME}/conda/condarc"
export PYLINTHOME="${XDG_CACHE_HOME}/pylint"
export CCACHE_CONFIGPATH="{$XDG_CONFIG_HOME}/ccache/ccache.config"
export CCACHE_DIR="${XDG_CACHE_HOME}/ccache"

# export R_USER="${XDG_CONFIG_HOME}/R"
export R_ENVIRON_USER="${XDG_CONFIG_HOME}/R/Renviron"
export R_PROFILE_USER="${XDG_CONFIG_HOME}/R/Rprofile"
export R_MAKEVARS_USER="${XDG_CONFIG_HOME}/R/Makevars"
# export R_HISTFILE="${XDG_DATA_HOME}/Rhistory"
export R_HISTSIZE=100000
export R_STARTUP_DEBUG=TRUE
export R_MINICONDA_PATH="$HOME/.local/rminiconda"
export PARALLEL_HOME="$XDG_CONFIG_HOME"/parallel
export PASSWORD_STORE_DIR="${XDG_DATA_HOME:-$HOME/.local/share}/password-store"
export TMUX_TMPDIR="$XDG_RUNTIME_DIR"
export ANDROID_SDK_HOME="${XDG_CONFIG_HOME:-$HOME/.config}/android"
export RIPGREP_CONFIG_PATH="${XDG_CONFIG_HOME:-$HOME/.config}/ripgrep/config"
export PYTHONSTARTUP="${XDG_CONFIG_HOME:-$HOME/.config}/python/pythonrc"
export PYTHONDONTWRITEBYTECODE=1
export TASKRC="${XDG_CONFIG_HOME:-$HOME/.config}/taskwarrior/taskrc"
export TASKOPENRC="${XDG_CONFIG_HOME:-$HOME/.config}/taskwarrior/taskopenrc"
export VIT_DIR="${XDG_CONFIG_HOME:-$HOME/.config}/vit"
export IPYTHONDIR="${XDG_CONFIG_HOME}/jupyter"
export PIPENV_VENV_IN_PROJECT=1
export COOKIECUTTER_CONFIG="${XDG_CONFIG_HOME}/python/cookiecutter.yaml"
export JUPYTER_CONFIG_DIR="${XDG_CONFIG_HOME}/jupyter"
export ENHANCD_DIR="${XDG_DATA_HOME}/enhancd"
export TRAVIS_CONFIG_PATH="${XDG_CONFIG_HOME}/travis"
export JULIA_PKGDIR="${XDG_LIB_HOME}/julia"
export NBRC_PATH="${XDG_CONFIG_HOME:-$HOME/.config}/nb/nbrc"
# export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
# export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export LEIN_HOME="${XDG_DATA_HOME}/lein"

# control themes of kde applications like okular, dolphin, and so on
# see: ~/.config/qt5ct
export QT_QPA_PLATFORMTHEME="qt5ct"

# new calibre 5.0 theme (moving to python 3)
export CALIBRE_USE_DARK_PALETTE=1

# Other program settings:
export SUDO_ASKPASS="$HOME/.local/bin/tools/dmenu/dmenupass"
export LESS=-R
export LESS_TERMCAP_mb="$(printf '%b' '[1;31m')"
export LESS_TERMCAP_md="$(printf '%b' '[1;36m')"
export LESS_TERMCAP_me="$(printf '%b' '[0m')"
export LESS_TERMCAP_so="$(printf '%b' '[01;44;33m')"
export LESS_TERMCAP_se="$(printf '%b' '[0m')"
export LESS_TERMCAP_us="$(printf '%b' '[1;32m')"
export LESS_TERMCAP_ue="$(printf '%b' '[0m')"
export MANPAGER="sh -c 'col -bx | bat -l man -p'"

# Start graphical server on tty1 if not already running.
[ "$(tty)" = "/dev/tty1" ] && ! pgrep -x Xorg >/dev/null && exec startx

# Switch escape and caps if tty and no passwd required:
sudo -n loadkeys "${XDG_CONFIG_HOME:-$HOME/.config}/xmodmap/ttymaps.kmap" 2>/dev/null

